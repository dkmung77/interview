﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using ProductList.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ProductList.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private IConfiguration configuration;
        private string apiUrl;
        private string apiKey;
        private HttpClient client = new HttpClient();

        public ProductController(IConfiguration iConfig)
        {
            configuration = iConfig;
            apiUrl = configuration.GetValue<string>("Rhipe:ApiUrl");
            apiKey = configuration.GetValue<string>("Rhipe:ApiKey");
            client.DefaultRequestHeaders.Add("api-key", apiKey);
        }

        // GET: api/<ProductController>
        [HttpGet]
        public async Task<IEnumerable<Product>> Get()
        {
            List<Product> products = new List<Product>();
            string path = string.Format("{0}/Products", apiUrl);
            HttpResponseMessage response = await client.GetAsync(path);
            
            if (response.IsSuccessStatusCode)
            {
                string strJson = await response.Content.ReadAsStringAsync();
                products = JsonConvert.DeserializeObject<List<Product>>(strJson);
             }
            return products;

        }

        // GET api/<ProductController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<ProductController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<ProductController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<ProductController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
