import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export class Home extends Component {
    static displayName = Home.name;

    render() {
        return (
            <div>
                <h1> Coding Exercise! </h1>
                <p>
                    Click <Link to="/product-list"> Here </Link> for product list
            </p>

            </div>
        );
    }
}
