﻿import React, { Component } from 'react';
import BootstrapTable from 'react-bootstrap-table-next';

import { useHistory, Link } from 'react-router-dom';

export class ProductList extends Component {

    constructor(props) {
        super(props);
        this.state = { products: [], loading: true, disableOrderBtn: true };
    }

    componentDidMount() {
        this.populateProduct();
    }

    onOrder = () => {
        this.props.history.push("/order-detail");
    }

    priceFormatter(cell) {
        if (cell) {
            return '$' + cell.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
        }

        return cell;
    }


    render() {

        const columns = [{
            dataField: 'productId',
            text: 'Product ID',
            sort: true
        }, {
            dataField: 'name',
            text: 'Product Name',
            sort: true
        }, {
            dataField: 'description',
            text: 'Description',
            sort: true,
            headerStyle: {
                width: '50%'
            }
        }, {
            dataField: 'unitPrice',
            text: 'Product Price',
            formatter: this.priceFormatter,
            sort: true
        }];

        const selectRow = {
            mode: 'checkbox',
            clickToSelect: true,
            style: { backgroundColor: '#c8e6c9' },
            onSelect: (row, isSelect, rowIndex, e) => {
                if (isSelect) {
                    this.setState({
                        disableOrderBtn: false
                    });
                } else {
                    if (this.node.selectionContext.selected.some(x => x !== row.productId)) {
                        this.setState({
                            disableOrderBtn: false
                        });
                    } else {
                        this.setState({
                            disableOrderBtn: true
                        });
                    }
                }
            },
            onSelectAll: (isSelect) => {
                this.setState({
                    disableOrderBtn: !isSelect
                });
            }
        };



        if (this.state.loading) {
            return (<p><em>Loading...</em></p>)
        } else {
            return (<div>
                <h1 id="tabelLabel" >Product List</h1>
                <div class="w-100 d-flex flex-row justify-content-between pb-2">
                    <p class="mt-1 mb-0">Please select product to order</p>
                    <button ref={n => this.button = n}
                        className="btn btn-primary"
                        onClick={this.onOrder.bind(this)}
                        disabled={this.state.disableOrderBtn}
                    >
                        Order
                        </button>
                </div>

                <BootstrapTable
                    bootstrap4
                    ref={n => this.node = n}
                    keyField='productId' data={this.state.products}
                    columns={columns}
                    selectRow={selectRow}
                    striped
                    hover
                    condensed
                    headerClasses="header-class"
                />

            </div>
            )
        }
    }

    async populateProduct() {
        debugger;
        const response = await fetch('api/product');
        const data = await response.json();
        this.setState({ products: data, loading: false });
    }
}


